<?php
declare(strict_types=1);

namespace Adapters;

use Insidesuki\Stamp\Adapters\AdapterConfig;
use Insidesuki\Utilities\File\Folder;
use PHPUnit\Framework\TestCase;

class AdapterConfigTest extends TestCase
{

    public function testOkCustomConfig():void{

        $adapterConfig = new AdapterConfig(Folder::fromPath('/var'));
        $adapterConfig->addExtraConfig('mensatek',new FakeCustomConfig('test1','/etc'));
        $this->assertInstanceOf(FakeCustomConfig::class,$adapterConfig->config('mensatek'));

    }



    public function testFailCustomConfigAlreadyExists():void{

        $this->expectExceptionMessage('CustomConfig mensatek,already exists!!!');
        $adapterConfig = new AdapterConfig(Folder::fromPath('/var/'));
        $adapterConfig->addExtraConfig('mensatek',new FakeCustomConfig('test1','/etc'));
        $adapterConfig->addExtraConfig('mensatek',[]);

    }

    public function testFailMissingConfig():void{

        $this->expectExceptionMessage('CustomConfig mensatek,does not exists!!!');
        $adapterConfig = new AdapterConfig(Folder::fromPath('/var/'));
        $adapterConfig->config('mensatek');

    }


}


class FakeCustomConfig{

    public readonly string $name;
    public readonly string $customPath;

    public function __construct(string $name, string $customPath)
    {
        $this->name = $name;
        $this->customPath = $customPath;
    }


}
