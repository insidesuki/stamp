<?php
declare(strict_types=1);

namespace Insidesuki\Stamp\Exceptions;

use RuntimeException;

class FolderDoesNotExistsException extends RuntimeException
{

    public function __construct(string $folder)
    {
        parent::__construct(sprintf('The directory:%s, does not exists',$folder));
    }

}