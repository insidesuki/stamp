<?php
declare(strict_types=1);

namespace Insidesuki\Stamp\Exceptions;

use Exception;

class UnableToCreateOutputException extends Exception
{

}