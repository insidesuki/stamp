<?php
declare(strict_types=1);

namespace Insidesuki\Stamp\Exceptions;

use Exception;

class FileDoesNotExistsException extends Exception
{

    public function __construct(string $message)
    {
        parent::__construct(sprintf('The file:%s, does not exists!!!',$message));
    }
}