<?php
declare(strict_types=1);

namespace Insidesuki\Stamp\Adapters\FakeAdapter;

use Insidesuki\Stamp\Adapters\AdapterConfig;
use Insidesuki\Stamp\Contracts\CustomConfig;
use Insidesuki\Stamp\Contracts\TsaAdapter;
use Insidesuki\Stamp\Exceptions\FileDoesNotExistsException;
use Insidesuki\Utilities\File\File;

class FakeTsaAdapter implements TsaAdapter
{

    /**
     * @throws FileDoesNotExistsException
     */
    public function stamp(File $file,AdapterConfig $config): File
    {

        $fileSigned = $file->folder->fullpath.'/'.$file->baseName;
        $fileResource = fopen($fileSigned, 'wb');
        $sign = '###SIGNED_AT_'.date('Y-m-d H');
        fwrite($fileResource,$sign);
        fclose($fileResource);


        return File::fromStamped($fileSigned, true, 'fake-info');

    }

    public function verify(File $file,?CustomConfig $customConfig = null): File
    {
        return $file;
    }
}