<?php
declare(strict_types=1);

namespace Insidesuki\Stamp\Adapters;

use Insidesuki\Utilities\File\File;
use Insidesuki\Utilities\File\Folder;
use RuntimeException;

/**
 * @author diemarc@protonmail.com
 */
class AdapterConfig
{

    private array $customConfig = [];

    public function __construct(
        public readonly Folder $outputPath ,
        public readonly string $algo = 'sha256',
        public readonly string $tsqFile = 'request.tsq',
        public readonly string $tsrFile = 'request.tsr',
    )
    {
    }

    public function addExtraConfig(string $name,mixed $config): void
    {

        if(array_key_exists($name,$this->customConfig)){
            throw new RuntimeException(sprintf('CustomConfig %s,already exists!!!', $name));
        }

        $this->customConfig[$name] = $config;

    }

    public function config(string $key): mixed
    {


        if (false === array_key_exists($key, $this->customConfig)) {
            throw new RuntimeException(sprintf('CustomConfig %s,does not exists!!!', $key));
        }

        return $this->customConfig[$key];

    }



}