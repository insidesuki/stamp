<?php

namespace Insidesuki\Stamp\Contracts;

interface TsaCredential
{

    public function baseUrl():string;
    public function username():string;
    public function password():string;

}