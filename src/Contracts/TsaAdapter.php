<?php

namespace Insidesuki\Stamp\Contracts;

use Insidesuki\Stamp\Adapters\AdapterConfig;
use Insidesuki\Utilities\File\File;

interface TsaAdapter
{

    public function stamp(File $file,AdapterConfig $config): File;

    /**
     * @param StampFile $file
     * @param CustomConfig|null $customConfig
     * @return StampFile
     */
    public function verify(File $file, ?CustomConfig $customConfig = null): File;

}